# Motorcontroller L298N & Motor

## Datenblatt

**Datasheet**

---

[Handsontec](https://www.handsontec.com/dataspecs/module/L298N%20Motor%20Driver.pdf)

**Anleitung**

---

[Reichelt](https://cdn-reichelt.de/documents/datenblatt/X200/SBC-MOTODRIVER2-ANLEITUNG.pdf)

**Datasheet deutsch**

---

[Reichelt](https://cdn-reichelt.de/documents/datenblatt/A300/SBC-MOTORDRIVER2_DATENBLATT_V2.pdf)

**Welche Umweltbedingungen müssen für die korrekte Anwendung des Bauteils herrschen?** 

Lagertemperatur: -20℃ - 135℃ 

Betriebstemperatur: -20℃ - 85°C 

Relative Luftfeuchtigkeit: 0% - 95%

Antriebsspannung: 5V - 35V

Antriebsstrom: 2A

Wiederstände und Lasten am Motor

**Unter welchen Umweltbedingungen ist mit fehlerhaften Daten/Verhalten zu rechnen? Was für technischen/bauliche Maßnahmen könnten wir ergreifen, um das zu verhindern? Welche Maßnahmen sind eventuell in unserem Programmcode sinnvoll?**

- Zu heiß/kalt: Fehlerhaftes Verhalten, Physische Beschädigung der Bauteile durch bspw. Schmelzen
- Zu feucht: Korrosion, Feuchtigkeit leitet (Kurzschlüsse)
- Bei Widerständen muss gegebenenfalls die Spannung am Motor erhöht werden. Wenn mehrere Motoren benutzt werden, sollte ebenfalls über Sensoren die Fahreigenschaften überprüft werden, da gerade bei billigen Motoren unterschiedliche Leistungen zu erwarten sind.
- Allgmein sollte darauf geachtet werden, dass die anliegende Spannung varieren kann

**Über welche Schnittstellen/Anschlüsse wird das Bauteil angesprochen?**

GPIO / mind. 5V

**Welche Pins am Raspberry Pi müssen wir dafür verwenden?**

Input 1: GPIO 26

Input 2: GPIO 20

![Nur PI](https://www.notion.so/image/https%3A%2F%2Fs3-us-west-2.amazonaws.com%2Fsecure.notion-static.com%2F000e0eea-8e25-4a94-8258-12fdeb3961c7%2F7CD35506-DA15-4672-9FAA-333251104C39_1_105_c.jpeg?table=block&id=6956839c-e4ba-48ec-9c71-3fa697966050&spaceId=e6e5b360-604a-44d3-a048-6f7a5500f0a3&width=2050&userId=&cache=v2)

![Pi mit extra Stromversorgung](https://www.notion.so/image/https%3A%2F%2Fs3-us-west-2.amazonaws.com%2Fsecure.notion-static.com%2F33924a23-ca37-4431-b07a-53784d026fe3%2FE759F0FC-838D-42F8-9A37-BCCE209018AE_1_105_c.jpeg?table=block&id=081a94b7-b5c5-49d8-a276-200fab9b18d8&spaceId=e6e5b360-604a-44d3-a048-6f7a5500f0a3&width=2050&userId=&cache=v2)

**Wie erfolgt die Spannungs- und Stromversorgung des Bauteils? Reicht die Versorgung durch den Raspberry Pi aus, oder benötigen wir eine Zusatzversorgung?**

Das Bauteil benötigt eine Spannung zwischen 5V und 35V. Um diese Sicher zu gewährleisten wird eine externe Stromversorgung empfohlen, da das PI selbst nur eine 5V Stromversorgung besitzt.

---

## **Programmierung**

**Sensor: Wie erhalten wir was für Daten vom Sensor?**

Wir haben keinen Sensor

**Wie (mit welchen Daten?) wird der Aktor angesteuert?**

Die Motorbrücke wird über zwei Pins angesteuert.

Dabei dreht sich der Motor vorwährts, wenn an Pin 26 eine hohe Spannung anliegt und rückwärts wenn an Pin 20 eine hohe Spannung anliegt.

**Gibt es eine Bibliothek für das Bauelement?**

Naja, pigpio zum Ansteuern

**Gibt es Beispielcode für die Verwendung des Bauelements?**

```python
import sys
import time
import RPi.GPIO as GPIO

mode=GPIO.getmode()
GPIO.cleanup()

Forward=26
Backward=20

GPIO.setmode(GPIO.BCM)
GPIO.setup(Forward, GPIO.OUT)
GPIO.setup(Backward, GPIO.OUT)

def forward(x):
	GPIO.output(Forward, GPIO.HIGH)
	print("Moving Forward")
	time.sleep(x)
	GPIO.output(Forward, GPIO.LOW)

def reverse(x):
	GPIO.output(Backward, GPIO.HIGH)
	print("Moving Backward")
	time.sleep(x)
	GPIO.output(Backward, GPIO.LOW)

while (1):
	forward(5)
	reverse(5)
	GPIO.cleanup()
```

**Entwickelt ein Programmgerüst für die Nutzung des Bauelementes in Kombination mit einem anderen Bauelement. Das Programmgerüst sollte lauffähig sein, die Bauelemente können/sollten als Mock-Objekte/-Funktionen vorliegen, dokumentiert die benötigten GPIO-Pins im Quellcode.**

```jsx
const Gpio = require("pigpio").Gpio;

const Forward = new Gpio(26, { mode: Gpio.OUTPUT });
const Backward = new Gpio(20, { mode: Gpio.OUTPUT });

async function forward(x) {
  Forward.digitalWrite(1);
  console.log("forward");
  await sleep(x);
  Forward.digitalWrite(0);
}

async function reverse(x) {
  Backward.digitalWrite(1);
  console.log("reverse");
  await sleep(x);
  Backward.digitalWrite(0);
}

async function go() {
  while (true) {
    await sleep(1000);
    await forward(3000);
    await sleep(1000);
    await reverse(3000);
  }
}

async function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

go();
```
